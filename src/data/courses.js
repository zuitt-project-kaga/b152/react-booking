let courseData = [
    {
        id:"wdc001",
        name:"PHP-Laravel",
        description:"Lorem, ipsum dolor sit amet consectetur adipisicing elit. Deserunt nihil magni a, esse temporibus vero inventore cumque, at ducimus doloribus voluptas. Ea ipsam non consequuntur quod inventore beatae animi aliquam!",
        price : 25000,
        onOffer:true
    },
    {
        id:"wdc002",
        name:"Python-Django",
        description:"Lorem, ipsum dolor sit amet consectetur adipisicing elit. Deserunt nihil magni a, esse temporibus vero inventore cumque, at ducimus doloribus voluptas. Ea ipsam non consequuntur quod inventore beatae animi aliquam!",
        price : 35000,
        onOffer:true
    },
    {
        id:"wdc003",
        name:"Java-Springboot",
        description:"Lorem, ipsum dolor sit amet consectetur adipisicing elit. Deserunt nihil magni a, esse temporibus vero inventore cumque, at ducimus doloribus voluptas. Ea ipsam non consequuntur quod inventore beatae animi aliquam!",
        price : 45000,
        onOffer:true
    },
    {
        id:"wdc004",
        name:"Next.js and ReactNative",
        description:"Lorem, ipsum dolor sit amet consectetur adipisicing elit. Deserunt nihil magni a, esse temporibus vero inventore cumque, at ducimus doloribus voluptas. Ea ipsam non consequuntur quod inventore beatae animi aliquam!",
        price : 100000,
        onOffer:false
    }
]

export default courseData;