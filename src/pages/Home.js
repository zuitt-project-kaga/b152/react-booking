import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home() {
  // let sampleProp = "I am sample data passed from Home component to Banner component"
  let sampleProp2 =
    "This sample data is passed from Home to Highlights component.";
  let bannerData = {
    title: "Zuitt Booking System B152",
    description: " View and book a course from our catalog!",
    buttonText: "View Our Course",
    destination:"/register"
  };
  return (
    <>
      <Banner bannerProp={bannerData}></Banner>
      <Highlights highligthProp={sampleProp2}></Highlights>
      {/* <Course></Course> */}
    </>
  );
}
