import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";
import Swal from "sweetalert2";

const AddCourse = () => {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  const token = localStorage.getItem("token");
  const isAdmin = localStorage.getItem("isAdmin");

  const createCourse = (e) => {
    e.preventDefault();
    // console.log(name,description,price);
    // console.log(token);
    console.log(isAdmin);
    fetch("http://localhost:4000/courses", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        name,
        description,
        price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data._id) {
          Swal.fire({
            icon: "success",
            title: "Add Course Successful",
          });
        } else {
          Swal.fire({
            icon: "error",
            title: "Add Course Failed",
            text: data.message,
          });
        }
      });
  };

  return (
    <>
      <h1 className=" my-5 text-center ">Add Course</h1>
      <Form onSubmit={(e) => createCourse(e)}>
        <Form.Group>
          <Form.Label>Name : </Form.Label>
          <Form.Control
            type="text"
            placeholder="Name..."
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          ></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Description : </Form.Label>
          <Form.Control
            type="text"
            placeholder="Description..."
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            required
          ></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Price : </Form.Label>
          <Form.Control
            type="number"
            placeholder="Price..."
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            required
          ></Form.Control>
        </Form.Group>
        <Button type="submit" className="my-5">
          Add Course
        </Button>
      </Form>
    </>
  );
};

export default AddCourse;
