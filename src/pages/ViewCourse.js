import { useState, useEffect, useContext } from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { useParams, Link } from "react-router-dom";
import Swal from "sweetalert2";
import CourseCard from "../components/Course";
import UserContext from "../userContext";

export default function ViewCourse() {
  //   console.log(useParams());

  const { courseId } = useParams();
  const { user } = useContext(UserContext);
  //   console.log(user);

  const [courseDetails, setCourseDetails] = useState({
    name: null,
    description: null,
    price: null,
  });

  useEffect(() => {
    fetch(`http://localhost:4000/courses/getSingleCourse/${courseId}`)
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        setCourseDetails({
          name: data.name,
          description: data.description,
          price: data.price,
        });
      });
  }, [courseId]);

  function enroll() {
    let token = localStorage.getItem("token");
    fetch("http://localhost:4000/users/enroll", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        courseId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.message === "Enrolled Successfully.") {
          Swal.fire({
            icon: "success",
            title: "Enrolled Successful",
          });
        } else {
          Swal.fire({
            icon: "error",
            title: "Enrollment Failed",
            text: data.message,
          });
        }
      });
  }

  return (
    <>
      <Row className="mt-5">
        <Col>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{courseDetails.name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{courseDetails.description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{courseDetails.price}</Card.Text>
            </Card.Body>
            {user.id && user.isAdmin === false ? (
              <Button onClick={enroll} variant="primary" className="btn-block">
                Enroll
              </Button>
            ) : (
              <Link className="btn btn-danger btn-block" to="/login">
                Login To Enroll
              </Link>
            )}
          </Card>
        </Col>
      </Row>

      {/* <h1 className="my-5 text-center">ViewCourse</h1>
     <CourseCard courseProp={courseDetails}></CourseCard> */}
    </>
  );
}
