import Banner from "../components/Banner";


export default function ErrorPage(second) {
    let errorBanner = {
        title: "Page Not Found",
        description: "The page your looking for does not exist.",
        buttonText: "Back to Home",
        destination:"/"
      };
    return <Banner bannerProp={errorBanner}></Banner>
}
