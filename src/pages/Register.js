import { useContext, useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import { Navigate } from "react-router-dom";
import UserContext from "../userContext";

export default function Register() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [isActive, setIsActive] = useState(false);

  const {user} = useContext(UserContext);
  //   const [success, setSuccess] = useState(false);

  //   console.log(firstName);
  //   console.log(lastName);
  //   console.log(email);
  //   console.log(mobileNo);
  //   console.log(password);
  //   console.log(confirmPassword);

  //   let myTimer = setTimeout(setSuccess(false), 3000);

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      mobileNo.length === 11 &&
      password !== "" &&
      confirmPassword !== "" && (
        password === confirmPassword
      )
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNo, password, confirmPassword]);

  const registerUser = async (e) => {
    try {
      e.preventDefault();
      await fetch("http://localhost:4000/users/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          firstName,
          lastName,
          email,
          mobileNo,
          password,
          confirmPassword,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          if (data.email) {
            Swal.fire({
              icon: "success",
              title: "Registration Successful",
              text: "Thank you for registration",
            });
            // alert("Thank you for rgistering!")
            window.location.href = "/login";
          } else {
            Swal.fire({
              icon: "error",
              title: "Registration Failed",
              text: "Something Went Wrong",
            });
            // alert("Something Went Wrong.")
          }
        });
      //   setSuccess(true);
      setFirstName("");
      setLastName("");
      setEmail("");
      setMobileNo("");
      setPassword("");
      setConfirmPassword("");
    } catch (error) {
      console.log(error.message);
    }

    // myTimer();
  };

  return (
    user.id 
    ?
    <Navigate to="/courses" replace={true}></Navigate>
    :
    <>
      <h1 className="my-5 text-center">Register</h1>
      <Form onSubmit={(e) => registerUser(e)}>
        <Form.Group>
          <Form.Label>First Name :</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter First Name"
            required
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Last Name :</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Last Name"
            required
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Email :</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter Email"
            required
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Mobile No :</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter 11 Digits No."
            required
            value={mobileNo}
            onChange={(e) => setMobileNo(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Password :</Form.Label>
          <Form.Control
            type="password"
            placeholder="Enter Password"
            required
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Confirm Password :</Form.Label>
          <Form.Control
            type="password"
            placeholder="Confirm Password"
            required
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
          ></Form.Control>
        </Form.Group>
        {isActive ? (
          <Button variant="primary" type="submit" className="my-5">
            something
          </Button>
        ) : (
          <Button variant="primary" disabled className="my-5">
            something
          </Button>
        )}
      </Form>
    </>
  );
}
