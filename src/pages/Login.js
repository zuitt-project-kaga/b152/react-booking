import React, { useState, useContext, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../userContext";
import { Navigate } from "react-router-dom";

const Login = () => {
  const { user, setUser } = useContext(UserContext);
  console.log(user);
  console.log(setUser);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [isActive, setIsActive] = useState(false);
  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  const onSubmitHandler = (e) => {
    e.preventDefault();
    fetch("http://localhost:4000/users/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.accessToken) {
          Swal.fire({
            icon: "success",
            title: "Login Successful",
            text: "Thank you for Login",
          });
          // alert("Thank you for rgistering!")
          localStorage.setItem("token", data.accessToken);
          //   localStorage.setItem('sample',"sample message");
          let token = localStorage.getItem("token");
          console.log(token);

          fetch("http://localhost:4000/users/getUserDetails", {
            method: "GET",
            headers: {
              "Authorization": `Bearer ${token}`,
            },
          })
            .then((res) => res.json())
            .then((data) => {
              // console.log(data);
              // localStorage.setItem('id',data._id);
              // localStorage.setItem('isAdmin',data.isAdmin);

              setUser({
                id: data._id,
                isAdmin: data.isAdmin,
              });
            });
        } else {
          Swal.fire({
            icon: "error",
            title: "Login Failed",
            text: data.message,
          });
          // alert("Something Went Wrong.")
        }
      });
  };

  return user.id ? (
    <Navigate to="/courses" replace={true} />
  ) : (
    <>
      <h1 className="my-5 text-center">Login</h1>
      <Form onSubmit={(e) => onSubmitHandler(e)}>
        <Form.Group>
          <Form.Label>Email :</Form.Label>
          <Form.Control
            type="email"
            required
            placeholder="Email..."
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          ></Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label>Password :</Form.Label>
          <Form.Control
            type="password"
            required
            placeholder="Password..."
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          ></Form.Control>
        </Form.Group>
        {isActive ? (
          <Button type="submit" className="my-5">
            Login
          </Button>
        ) : (
          <Button disabled className="my-5">
            Login
          </Button>
        )}
      </Form>
    </>
  );
};

export default Login;
