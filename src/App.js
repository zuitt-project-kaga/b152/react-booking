import React from "react";
import { Container } from "react-bootstrap";
import "./App.css";
import AppNavBar from "./components/AppNavBar";
import Courses from "./pages/Courses";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Home from "./pages/Home";
import { BrowserRouter as Router } from "react-router-dom";
import { Routes, Route } from "react-router-dom";
import ErrorPage from "./pages/ErrorPage";
import AddCourse from "./pages/AddCourse";
import { UserProvider } from "./userContext";
import { useEffect, useState } from "react";
import Logout from "./pages/Logout";
import ViewCourse from "./pages/ViewCourse";

export default function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });
  useEffect(() => {
    fetch("http://localhost:4000/users/getUserDetails", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  }, []);

  const unsetUser = () => {
    localStorage.clear();
  }

  // console.log(user);

  // let sample1 = "I'm a sample data";
  return (
    <>
      <UserProvider value={{ user, setUser,unsetUser }}>
        <Router>
          <AppNavBar></AppNavBar>
          <Container>
            <Routes>
              <Route path="/" element={<Home></Home>} />
              <Route path="/courses" element={<Courses></Courses>}></Route>
              <Route path="/courses/viewCourse/:courseId" element={<ViewCourse></ViewCourse>}></Route>
              <Route path="/login" element={<Login></Login>}></Route>
              <Route path="/register" element={<Register></Register>}></Route>
              <Route
                path="/addCourse"
                element={<AddCourse></AddCourse>}
              ></Route>
              <Route path="/logout" element={<Logout></Logout>}></Route>
              <Route path="*" element={<ErrorPage></ErrorPage>}></Route>
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}
