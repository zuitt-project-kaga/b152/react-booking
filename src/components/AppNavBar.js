import React from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { useContext } from "react";
import UserContext from "../userContext";

const AppNavBar = () => {
  // console.log(useContext(UserContext));
  const { user } = useContext(UserContext);
  // console.log(user);
  return (
    <>
      <Navbar bg="primary" expand="lg">
        <Container fluid>
          <Navbar.Brand href="#home">B152 Booking</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav"></Navbar.Toggle>
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              <Nav.Link href="/">Home</Nav.Link>
              <Nav.Link href="/courses">Course</Nav.Link>
              {user.id ? (
                user.isAdmin ? (
                  <>
                    <Nav.Link href="/addCourse">Add Course</Nav.Link>
                    <Nav.Link href="/logout">Logout</Nav.Link>
                  </>
                ) : (
                  <Nav.Link href="/logout">Logout</Nav.Link>
                )
              ) : (
                <>
                  <Nav.Link href="/register">Register</Nav.Link>
                  <Nav.Link href="/login">Login</Nav.Link>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default AppNavBar;
