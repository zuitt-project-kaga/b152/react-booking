import React from "react";
import { Col, Row } from "react-bootstrap";

const About = () => {
  return (
    <Row>
      <Col className="p-5 bg-danger">
        <h1 className="my-5">About Me</h1>
        <h2 className="mt-3">Masahiro Kaga</h2>
        <h3>Full Stack Web Developer</h3>
        <p className="mb-4">
          Hi there, I am Jr webdeveloper and I am good at React.js and Next.js
        </p>
        <h4>Contact Me</h4>
        <ul>
          <li>Email : masahiro@gmail.com</li>
          <li>Mobile No : 60484231**</li>
          <li>Address : Abashirigun bihorocho 123</li>
        </ul>
      </Col>
    </Row>
  );
};

export default About;
