import { Row, Col, Card } from "react-bootstrap";

export default function Highlights({highligthProp}) {
    // console.log(highligthProp);
  return (
    <Row className="my-3">
      <Col xs={12} md={4}>
        <Card className="p-3 cardHighlight">
          <Card.Body>
            <Card.Title>
              <h2>Learn From Home</h2>
              <Card.Text>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                Sapiente id!
              </Card.Text>
            </Card.Title>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4}>
        <Card className="p-3 cardHighlight">
          <Card.Body>
            <Card.Title>
              <h2>Study Now, Pay Later</h2>
              <Card.Text>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                Sapiente voluptates numquam iusto exercitationem natus ipsum
                atque rerum? Similique nulla illum natus voluptas. Rem expedita
                deserunt ut voluptatum hic! Atque, id!
              </Card.Text>
            </Card.Title>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4}>
        <Card className="p-3 cardHighlight">
          <Card.Body>
            <Card.Title>
              <h2>Be Part of Our Community</h2>
              <Card.Text>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                Sapiente voluptates numquam iusto exercitationem natus ipsum
                atque rerum? Similique nulla illum natus voluptas. Rem expedita
                deserunt ut voluptatum hic! Atque, id!
              </Card.Text>
            </Card.Title>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
